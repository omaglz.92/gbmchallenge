//
//  IPCDataTest.swift
//  GBMPruebaTests
//
//  Created by omar hernandez on 23/08/21.
//
import XCTest
class IPCDataTest: XCTestCase {
    lazy var promise = XCTestExpectation(description: "Completion handler invoked")
    func testURL() {
        let manager: DCConectionManager = DCConectionManager()
        let url: String = DCUrl.URL.main + DCUrl.EndPoints.getIPC
        let request: DCRequest = DCRequest(url: url, header: [:], method: .get, body: nil)
        manager.delegate = self
        manager.conectionWithRequest(serviceName: String(describing: DCGBMResponse.self), request: request)
        wait(for: [promise], timeout: 5)
    }
}
extension IPCDataTest: DCConectionManagerDelegate {
    func receivedData(data: Data, serviceName: String) {
        switch serviceName {
        case String(describing: DCGBMResponse.self):
            do {
                let _ = try JSONDecoder().decode(DCGBMResponse.self, from: data)
                print("Funciona")
                promise.fulfill()
            } catch let error {
                XCTFail("Error: \(error.localizedDescription)")
            }
        default: break
        }
    }
    func receivedError(data: Data?, error: Error?, serviceName: String) {
        XCTFail("Error: \(error.debugDescription)")
    }
}

