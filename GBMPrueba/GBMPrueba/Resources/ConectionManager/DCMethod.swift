//
//  DCMethod.swift
//  GBMPrueba
//
//  Created by omar hernandez on 21/08/21.
//

import Foundation
public enum DCMethod: String {
    case post = "POST"
    case get = "GET"
    case put = "PUT"
    case patch = "PATCH"
    case delete = "DELETE"
}
