//
//  DCConectionManager.swift
//  GBMPrueba
//
//  Created by omar hernandez on 21/08/21.
//
import Foundation
public protocol DCConectionManagerDelegate: AnyObject {
    func receivedData(data: Data, serviceName: String)
    func receivedError(data: Data?, error: Error?, serviceName: String)
}
public class DCConectionManager: NSObject {
    internal let timeout: TimeInterval = 30.0
    private var manager: URLSession?
    public var delegate: DCConectionManagerDelegate?
    public override init() {
        super.init()
        let defaultSettings: URLSessionConfiguration = URLSessionConfiguration.default
        defaultSettings.timeoutIntervalForRequest = timeout
        defaultSettings.timeoutIntervalForResource = timeout
        self.manager = URLSession(configuration: defaultSettings, delegate: nil, delegateQueue: nil)
    }
    func conectionWithRequest(serviceName: String, request: DCRequest) {
        do {
            let urlrequest: URLRequest = try DCRequest.configureRequest(url: request.url, header: request.header, parameters: request.parameters, method: request.method, body: request.body, timeout: timeout)
            self.manager?.dataTask(with: urlrequest, completionHandler: { (data, response, error) in
                let responseS: String = String(data: data ?? Data(), encoding: .utf8) ?? ""
                #if DEBUG
                print("Response: \(String(describing: responseS)) - serviceName: \(serviceName)")
                #endif
                if error != nil {
                    self.delegate?.receivedError(data: data, error: error, serviceName: serviceName)
                    return
                }
                if let responseServer = response as? HTTPURLResponse, let data = data {
                    switch responseServer.statusCode {
                    case 200...299:
                        DispatchQueue.main.async {
                            self.delegate?.receivedData(data: data, serviceName: serviceName)
                        }
                    default:
                        DispatchQueue.main.async {
                            self.delegate?.receivedError(data: data, error: error, serviceName: serviceName)
                        }
                    }
                }
            }).resume()
        } catch let error {
            DispatchQueue.main.async {
                self.delegate?.receivedError(data: nil, error: error, serviceName: serviceName)
            }
        }
    }
}
public enum DCError: String, Error {
    case wrongURL
}
