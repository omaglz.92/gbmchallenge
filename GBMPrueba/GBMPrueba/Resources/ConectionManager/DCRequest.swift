//
//  DCRequest.swift
//  GBMPrueba
//
//  Created by omar hernandez on 21/08/21.
//
import Foundation
public class DCRequest {
    public var url: String
    public var header: [String: String]?
    public var parameters: [String: Any]?
    public var method: DCMethod
    public var body: Data?
    init(url: String, header: [String: String]?, parameters: [String: Any]? = [:], method: DCMethod, body: Data?) {
        self.url = url
        self.header = header
        self.method = method
        self.body = body
        self.parameters = parameters
    }
    static func configureRequest(url: String, header: [String: String]?, parameters: [String: Any]? = [:], method: DCMethod, body: Data?, timeout: TimeInterval = 30.0) throws -> URLRequest {
        guard let url = URL(string: url) else {
            throw DCError.wrongURL
        }
        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: timeout)
        request.httpBody = body
        request.httpMethod = method.rawValue
        for (kind, value) in header ?? [:] {
            request.addValue(value, forHTTPHeaderField: kind)
        }
        #if DEBUG
        print("url: \(url)")
        print("header: \(header?.description ?? "")")
        print("method: \(method)")
        print("parameters: \(parameters?.description ?? "")")
        let responseS: String = String(data: body ?? Data(), encoding: .utf8) ?? ""
        print("body: \(responseS)")
        #endif
        return request
    }
}
