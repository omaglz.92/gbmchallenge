//
//  DCBiometrics.swift
//  GBMPrueba
//
//  Created by omar hernandez on 21/08/21.
//

import Foundation
import LocalAuthentication
import UIKit
protocol DCBiometricsDelegate {
    /// Función de autenticación exitosa
    func authenticationSuccess()
    /// Función de autenticación fallida
    /// - Parameter error: error
    func authenticationFail(error: Error?)
}
open class DCBiometrics: NSObject {
    var delegate: DCBiometricsDelegate?
    public static let shared = DCBiometrics()
    /// Varifica que podamos usar biometrics
    static func canUseBiometrics() -> Bool {
        var isAvailable = false
        var error: NSError?
        if LAContext().canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            isAvailable = true
        }
        return isAvailable
    }
    /// Verifica si podemos usar faceID
    func canUseFaceID() -> Bool {
        if #available(iOS 11.0, *) {
            let context = LAContext()
            if context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthentication, error: nil) {
                if context.biometryType == LABiometryType.faceID {
                    return true
                } else {
                    return false
                }
            } else {
                return false
            }
        }
        return false
    }
    /// Función para autenticación biometrica
    /// - Parameters:
    ///   - message: Mensaje en alerte a de autenticación
    ///   - fallbackTitle: Es el mensaje para el botón de la acción fallback
    ///   - cancelTitle: Es el mensaje para el botón de cancelar
    func authenticate(message: String, fallbackTitle: String? = "", cancelTitle: String? = "") {
        let reasonString = message.isEmpty ? DCBiometrics.shared.defaultBiometricAuthenticationReason() : message
        let context = LAContext()
        context.localizedFallbackTitle = fallbackTitle
        if #available(iOS 10.0, *) {
            context.localizedCancelTitle = cancelTitle
        }
        DCBiometrics.shared.evaluate(policy: LAPolicy.deviceOwnerAuthentication, with: context, reason: reasonString)
    }
    /// Método que evalua la politica de autenticación.
    func evaluate(policy: LAPolicy, with context: LAContext, reason: String) {
        context.evaluatePolicy(policy, localizedReason: reason) { (success, error) in
            DispatchQueue.main.async {
                if success {
                    self.delegate?.authenticationSuccess()
                } else {
                    self.delegate?.authenticationFail(error: error)
                }
            }
        }
    }
    /**
     Mensaje predeterminado en caso de que no se establezca ningúno.
     - returns: Es una cadena con el mensaje de default.
     */
    func defaultBiometricAuthenticationReason() -> String {
        return canUseFaceID() ? "DCBiometricsDelegate.confirmFaceID".localize : "DCBiometricsDelegate.confirmTouchId".localize
    }
}
