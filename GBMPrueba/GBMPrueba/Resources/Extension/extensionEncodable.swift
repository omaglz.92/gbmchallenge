//
//  extensionEncodable.swift
//  GBMPrueba
//
//  Created by omar hernandez on 21/08/21.
//

import Foundation
public extension Encodable {
    var toData: Data? {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        encoder.dateEncodingStrategy = .iso8601
        guard let jsonData = try? encoder.encode(self) else {
            return nil
        }
        return jsonData
    }
    var dictionary: [String: Any]? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
    }
    var paramsStringData: Data? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        let dictionary = (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] } ?? [:]
        var paramString: String = ""
        for (kind, value) in dictionary {
            if paramString != "" {
                paramString += "&"
            }
            paramString+="\(kind)=\(value)"
        }
        print(paramString)
        return Data(paramString.utf8)
    }
    var toParamsString: String? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        let dictionary = (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] } ?? [:]
        var paramString: String = ""
        for (kind, value) in dictionary {
            if paramString != "" {
                paramString += "&"
            }
            paramString+="\(kind)=\(value)"
        }
        return paramString
    }
}
