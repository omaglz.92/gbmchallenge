//
//  extensionDate.swift
//  GBMPrueba
//
//  Created by omar hernandez on 22/08/21.
//

import Foundation
extension Date {
    func toString(format: String = "dd-MM-yy HH:mm:ss") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let str = dateFormatter.string(from: self)
        return str
    }
}
