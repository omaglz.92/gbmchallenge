//
//  extensionColor.swift
//  GBMPrueba
//
//  Created by omar hernandez on 22/08/21.
//
import UIKit
extension UIColor {
    /// Color de fondo
    static var DCBackGoundColor = UIColor(named: "DCBackGoundColor") ?? .black
    /// Color de vistas de contenido
    static var DCBackContent = UIColor(named: "DCBackContent") ?? .black
    /// Color verde
    static var DCGreen = UIColor(named: "DCGreen") ?? .green
    /// Color texto Primario
    static var DCPrimaryText = UIColor(named: "DCPrimaryText") ?? .white
    /// Color Rojo
    static var DCRed = UIColor(named: "DCRed") ?? .red
    /// Color de sombra
    static var DCShadow = UIColor(named: "DCShadow") ?? .gray
}
