//
//  extensionString.swift
//  GBMPrueba
//
//  Created by omar hernandez on 22/08/21.
//
import Foundation
extension String {
    /// Devuelve el valor localizado en el proyecto
    var localize: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    /// Convierte la cadena de texto a fecha
    /// - Parameter format: Formato de fecha
    /// - Returns: Devuelve la fecha
    func toDate(format: String = "yyyy-MM-dd'T'HH:mm:ss.SSSz") -> Date? {
        let formater = DateFormatter()
        formater.dateFormat = format
        let date = formater.date(from: self)
        return date
    }
}

