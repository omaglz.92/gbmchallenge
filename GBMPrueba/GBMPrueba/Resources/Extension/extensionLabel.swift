//
//  extensionLabel.swift
//  GBMPrueba
//
//  Created by omar hernandez on 22/08/21.
//

import UIKit
/// Extensión para localizable de UILabel
extension UILabel {
    /// Agrega opción al inspector del texto
    /// - Parameters:
    ///   - localizableText: Tag del texto perteneciente al texto de la etiqueta
    @IBInspectable var localizableText: String? {
        get { return nil }
        set(localizableText) {
            self.text = localizableText?.localize
        }
    }
}
