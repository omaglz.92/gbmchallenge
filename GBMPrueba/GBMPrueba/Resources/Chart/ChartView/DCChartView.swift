//
//  DCChartView.swift
//  GBMPrueba
//
//  Created by omar hernandez on 21/08/21.
//

import UIKit

class DCChartView: UIView {
    @IBOutlet private weak var contentView: DCChartView!
    @IBOutlet private weak var viewAxisY: UIView!
    @IBOutlet private weak var viewAxisYRight: UIView!
    @IBOutlet private weak var viewAxisX: UIView!
    @IBOutlet private weak var viewLineChart: DCLineChartView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblFooter: UILabel!
    @IBOutlet weak var viewIndicatorFooter: UIView!
    /// Cantidad de lineas y etiquetas horizontales(eje Y) que dividirá la gráfica
    var linesYAxis: Int = 5
    /// Valor mínimo a representar eje Y
    private var minYValue: Double = 0
    /// Valor máximo a representar eje Y
    private var maxYValue: Double = 0
    /// Valor máximo a representar eje Y con margen
    private var maxYAxisValue: Double = 0
    /// Valor mínimo a representar eje Y con margen
    private var minYAxisValue: Double = 0
    /// Valor máximo a representar eje X con margen
    private var maxXAxisValue: Double = 0
    /// Diferencia en Y entre valores
    private var diferenceY: Double = 0
    /// Diferencia en X entre valores
    private var diferenceX: Double = 0
    /// Color de etiquetas
    var labelColor: UIColor = UIColor.black {
        didSet {
            lblFooter.textColor = labelColor
            lblTitle.textColor = labelColor
        }
    }
    /// Fuente de etiquetas
    var labelIndicatorFont: UIFont = UIFont.systemFont(ofSize: 12)
    /// Color de gráfica
    var chartColor: UIColor = UIColor.red
    /// Color de lineas de secciones
    var linesColor: UIColor = UIColor.lightGray
    var data: DCChartData?
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    private func commonInit() {
        Bundle.main.loadNibNamed("DCChartView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        initDraw()
    }
    func initDraw() {
        guard let dataArray = data else { return }
        let sorted = dataArray.values.sorted(by: {$0 > $1})
        let auxDifY = ((sorted.first ?? 0.0)-(sorted.last ?? 0.0))
        maxYAxisValue = (sorted.first ?? 0.0)+auxDifY/2
        minYAxisValue = (sorted.last ?? 0.0)-auxDifY/2
        maxXAxisValue = Double(sorted.count)
        diferenceY = maxYAxisValue-minYAxisValue
        diferenceX = maxXAxisValue
        minYValue = sorted.last ?? 0.0
        maxYValue = sorted.first ?? 0.0
        lblTitle.text = dataArray.title
        lblFooter.text = dataArray.footer
        viewIndicatorFooter.backgroundColor = chartColor
        drawAxisY()
        viewLineChart.chartColor = chartColor
        viewLineChart.linesColor = linesColor
        viewLineChart.data = dataArray
        viewLineChart.setNeedsDisplay()
    }
    /// Pinta el eje Y
    private func drawAxisY() {
        let _ = viewAxisY.subviews.map({$0.removeFromSuperview()})
        let _ = viewAxisYRight.subviews.map({$0.removeFromSuperview()})
        for value in 0...linesYAxis {
            addLabelInY(view: viewAxisY, value: value, alignment: .right)
            addLabelInY(view: viewAxisYRight, value: value, alignment: .left)
        }
    }
    func addLabelInY(view: UIView, value: Int, alignment: NSTextAlignment) {
        let height = view.bounds.height
        let lineValue = maxYAxisValue-(diferenceY/Double(linesYAxis))*Double(value)
        let label = configureAxisLabel(text: "\(Int(lineValue))", alignment: alignment)
        view.addSubview(label)
        let positionY = getPosition(size: height, minValue: minYAxisValue, maxValue: maxYAxisValue, currentValue: lineValue)
        label.heightAnchor.constraint(equalToConstant: 20).isActive = true
        label.topAnchor.constraint(equalTo: view.topAnchor, constant: positionY-10).isActive = true
        label.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        label.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }
    func configureAxisLabel(text: String, alignment: NSTextAlignment) -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = labelColor
        label.text = text
        label.font = labelIndicatorFont
        label.textAlignment = alignment
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.7
        return label
    }
    /// Regresa la posisión en la vista
    /// - Parameters:
    ///   - size: Tamaño de la vista
    ///   - minValue: Minímo valor a representar en la gráfica
    ///   - maxValue: Máximo valor a representar en la gráfica
    ///   - currentValue: Valor a dibujar
    /// - Returns: Valor de la posición
    private func getPosition(size: CGFloat, minValue: Double, maxValue: Double, currentValue: Double) -> CGFloat {
        let diference = maxValue-minValue
        let value = size/CGFloat(diference)
        return size-(CGFloat(currentValue-minValue)*value)
    }
}
