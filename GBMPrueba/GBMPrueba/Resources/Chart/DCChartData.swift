//
//  DCChartData.swift
//  GBMPrueba
//
//  Created by omar hernandez on 21/08/21.
//

import Foundation
struct DCChartData {
    var title: String
    var values: [Double]
    var footer: String
}
