//
//  DCLineChartView.swift
//  GBMPrueba
//
//  Created by omar hernandez on 21/08/21.
//

import UIKit
final class DCLineChartView: UIView {
    @IBOutlet private weak var contentView: UIView!
    /// Cantidad de lineas y etiquetas horizontales(eje Y) que dividirá la gráfica
    var linesYAxis: Int = 5
    /// Valor mínimo a representar eje Y
    private var minYValue: Double = 0
    /// Valor máximo a representar eje Y
    private var maxYValue: Double = 0
    /// Valor máximo a representar eje Y con margen
    private var maxYAxisValue: Double = 0
    /// Valor mínimo a representar eje Y con margen
    private var minYAxisValue: Double = 0
    /// Valor máximo a representar eje X con margen
    private var maxXAxisValue: Double = 0
    /// Diferencia en Y entre valores
    private var diferenceY: Double = 0
    /// Diferencia en X entre valores
    private var diferenceX: Double = 0
    /// Línae para la grafica
    private var singlePath: UIBezierPath?
    /// Valores a pintar
    private var dataValues: [Double] = []
    /// color de gráfica
    var chartColor: UIColor = UIColor.black
    /// Color de lineas de secciones
    var linesColor: UIColor = UIColor.lightGray
    /// Ingresa la información a la vista
    var data: DCChartData?
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        guard data != nil else { return }
        initDraw()
    }
    /// Valida y comienza pintar la grafica
    func initDraw() {
        guard let dataArray = data else { return }
        dataValues = dataArray.values
        let sorted = dataArray.values.sorted(by: {$0 > $1})
        let auxDifY = ((sorted.first ?? 0.0)-(sorted.last ?? 0.0))
        maxYAxisValue = (sorted.first ?? 0.0)+auxDifY/2
        minYAxisValue = (sorted.last ?? 0.0)-auxDifY/2
        maxXAxisValue = Double(sorted.count)
        diferenceY = maxYAxisValue-minYAxisValue
        diferenceX = maxXAxisValue
        minYValue = sorted.last ?? 0.0
        maxYValue = sorted.first ?? 0.0
        drawChart()
        drawHorizontalLines()
        drawBorderLines()
    }
    func drawBorderLines() {
        let horizontalLine = UIBezierPath()
        horizontalLine.move(to: CGPoint(x: 0.0, y: 0.0))
        horizontalLine.addLine(to: CGPoint(x: self.bounds.width, y: 0.0))
        horizontalLine.addLine(to: CGPoint(x: self.bounds.width, y: self.bounds.height))
        linesColor.setStroke()
        horizontalLine.lineWidth = 5
        horizontalLine.stroke()
    }
    /// Pinta la lineas que forman la gráfica.
    private func drawChart() {
        singlePath = nil
        singlePath = UIBezierPath()
        singlePath?.lineWidth = 1
        let width = self.frame.width/CGFloat(maxXAxisValue)
        let height = self.frame.height
        for (index, value) in dataValues.enumerated() {
            let posX = CGFloat(index)*width
            let position = getPosition(size: height, minValue: minYAxisValue, maxValue: maxYAxisValue, currentValue: value)
            let posY = height-position
            if index == 0 {
                singlePath?.move(to: CGPoint(x: posX, y: posY))
            } else {
                singlePath?.addLine(to: CGPoint(x: posX, y: posY))
            }
        }
        chartColor.setStroke()
        singlePath?.stroke()
    }
    /// Crea las lineas horizontales
    func drawHorizontalLines() {
        let height = self.bounds.height
        for value in 1...linesYAxis {
            let positionY = CGFloat(value)*height/CGFloat(linesYAxis)
            horizontalLine(yValue: positionY)
        }
    }
    /// Pinta las etiquetas y lienas verticales en la vista.
    /// - Parameter yValue: Posisión de valor en Y
    private func horizontalLine(yValue: CGFloat) {
        let horizontalLine = UIBezierPath()
        linesColor.setStroke()
        horizontalLine.lineWidth = 1
        horizontalLine.move(to: CGPoint(x: 0.0, y: yValue))
        horizontalLine.addLine(to: CGPoint(x: self.bounds.width, y: yValue))
        horizontalLine.stroke()
    }
    /// Regresa la posisión en la vista
    /// - Parameters:
    ///   - size: Tamaño de la vista
    ///   - minValue: Minímo valor a representar en la gráfica
    ///   - maxValue: Máximo valor a representar en la gráfica
    ///   - currentValue: Valor a dibujar
    /// - Returns: Valor de la posición
    private func getPosition(size: CGFloat, minValue: Double, maxValue: Double, currentValue: Double) -> CGFloat {
        let diference = maxValue-minValue
        let value = size/CGFloat(diference)
        return CGFloat(currentValue-minValue)*value
    }
}
