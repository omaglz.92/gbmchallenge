//
//  DCBaseVC.swift
//  GBMPrueba
//
//  Created by omar hernandez on 22/08/21.
//

import Foundation
import UIKit
protocol DCViewBaseProtocols {
    func showMessage(message: String)
    func showWait()
    func hideWait()
}
class DCBaseVC: UIViewController {
    lazy var myActivityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(style: .large)
        indicator.hidesWhenStopped = true
        indicator.backgroundColor = .DCBackContent
        indicator.color = .DCPrimaryText
        indicator.layer.shadowColor = UIColor.DCShadow.cgColor
        indicator.layer.shadowOpacity = 1
        indicator.layer.shadowOffset = .zero
        indicator.layer.shadowRadius = 4
        indicator.translatesAutoresizingMaskIntoConstraints = false
        return indicator
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.DCBackGoundColor
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.barTintColor = UIColor.DCBackGoundColor
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.DCPrimaryText
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.DCPrimaryText]
        configureActivity()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    fileprivate func configureActivity() {
        view.addSubview(myActivityIndicator)
        myActivityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        myActivityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        myActivityIndicator.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.3).isActive = true
        myActivityIndicator.heightAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.3).isActive = true
        myActivityIndicator.layer.cornerRadius = 6
    }
    func showMessage(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "common.title.accept".localize, style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    func showMessage(message: String) {
        showMessage(title: "", message: message)
    }
    func showWait() {
        myActivityIndicator.startAnimating()
    }
    func hideWait() {
        myActivityIndicator.stopAnimating()
    }
}

