//
//  DCStrings.swift
//  GBMPrueba
//
//  Created by omar hernandez on 21/08/21.
//
import Foundation
struct DCUrl {
    struct URL {
        static let main = "https://run.mocky.io"
    }
    struct EndPoints {
        static let getIPC = "/v3/cc4c350b-1f11-42a0-a1aa-f8593eafeb1e"
    }
}
