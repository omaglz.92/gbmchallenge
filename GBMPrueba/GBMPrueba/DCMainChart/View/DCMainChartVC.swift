//
//  DCMainChartVC.swift
//  GBMPrueba
//
//  Created by omar hernandez on 21/08/21.
//  
//
import UIKit
final class DCMainChartVC: DCBaseVC {
    var presenter: DCMainChartPresenter?
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var viewChart: DCChartView!
    let cellReuseIdentifier = "DCMainChartCell"
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.getIPC()
        configureTable()
        configureChart()
    }
    func configureTable() {
        tableView.register(UINib(nibName: cellReuseIdentifier, bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
    }
    func configureChart() {
        viewChart.backgroundColor = .DCBackGoundColor
        viewChart.labelColor = .DCPrimaryText
        viewChart.linesColor = .lightGray
        viewChart.chartColor = .DCPrimaryText
    }
}
extension DCMainChartVC: DCMainChartViewProtocol {
    func getIPCSuccess() {
        guard let dataIPC = presenter?.dataArrayIPC else { return }
        tableView.reloadData()
        var values: [Double] = [Double]()
        for item in dataIPC {
            values.append(item.price)
        }
        viewChart.data = DCChartData(title: "Omar", values: values, footer: "Precio")
        viewChart.setNeedsDisplay()
    }
}
extension DCMainChartVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let dataIPC = presenter?.dataArrayIPC else { return 0 }
        return dataIPC.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let dataIPC = presenter?.dataArrayIPC?[indexPath.row],
              let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as? DCMainChartCell else {
            return UITableViewCell()
        }
        cell.configureCell(dataIPC: dataIPC)
        return cell
    }
}
