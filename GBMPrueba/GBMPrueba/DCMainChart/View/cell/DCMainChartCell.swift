//
//  DCMainChartCell.swift
//  GBMPrueba
//
//  Created by omar hernandez on 22/08/21.
//

import UIKit

final class DCMainChartCell: UITableViewCell {
    @IBOutlet private weak var backView: UIView!
    @IBOutlet private weak var lblDate: UILabel!
    @IBOutlet private weak var lblvolume: UILabel!
    @IBOutlet private weak var lblPercentChance: UILabel!
    @IBOutlet private weak var lblChance: UILabel!
    @IBOutlet private weak var lblPrice: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func configureCell(dataIPC: DCGBMResponseElement) {
        lblDate.attributedText = setAttributes(title: "DCMainChartCell.date".localize, value: dataIPC.date.toDate()?.toString() ?? "-")
        lblvolume.attributedText = setAttributes(title: "DCMainChartCell.volume".localize, value: "\(dataIPC.volume)")
        lblPercentChance.attributedText = setAttributes(title: "DCMainChartCell.percent".localize, value: "\(dataIPC.percentageChange)")
        lblPrice.text = "$\(dataIPC.price)"
        if dataIPC.change > 0.0 {
            lblChance.text = "+$\(dataIPC.change)"
            lblChance.textColor = .DCGreen
        } else {
            lblChance.text = "$\(dataIPC.change)"
            lblChance.textColor = .DCRed
        }
        configureContentView()
    }
    func configureContentView() {
        backView.layer.borderWidth = 1
        backView.layer.cornerRadius = 6
        backView.layer.borderColor = UIColor.clear.cgColor
        backView.layer.shadowColor = UIColor.DCShadow.cgColor
        backView.layer.shadowOpacity = 1
        backView.layer.shadowOffset = CGSize(width: 0, height: 1)
        backView.layer.shadowRadius = 2
    }
    func setAttributes(title: String, value: String) -> NSAttributedString {
        let attributes: NSMutableAttributedString = NSMutableAttributedString()
        attributes.append(NSAttributedString(string: title, attributes: [.font: UIFont.boldSystemFont(ofSize: 12)]))
        attributes.append(NSAttributedString(string: value, attributes: [.font: UIFont.systemFont(ofSize: 12)]))
        return attributes
    }
}
