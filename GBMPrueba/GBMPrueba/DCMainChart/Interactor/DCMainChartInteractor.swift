//
//  DCMainChartInteractor.swift
//  GBMPrueba
//
//  Created by omar hernandez on 21/08/21.
//  
//
import Foundation
final class DCMainChartInteractor: DCMainChartInteractorInputProtocol {
    var presenter: DCMainChartInteractorOutputProtocol?
    @objc func getIPC() {
        let manager: DCConectionManager = DCConectionManager()
        let url: String = DCUrl.URL.main + DCUrl.EndPoints.getIPC
        let request: DCRequest = DCRequest(url: url, header: [:], method: .get, body: nil)
        manager.delegate = self
        manager.conectionWithRequest(serviceName: String(describing: DCGBMResponse.self), request: request)
    }
}
extension DCMainChartInteractor: DCConectionManagerDelegate {
    func receivedData(data: Data, serviceName: String) {
        switch serviceName {
        case String(describing: DCGBMResponse.self):
            do {
                let entity = try JSONDecoder().decode(DCGBMResponse.self, from: data)
                presenter?.getIPCSuccess(entity: entity)
            } catch let error {
                presenter?.getIPCFail(message: error.localizedDescription)
            }
        default: break
        }
    }
    func receivedError(data: Data?, error: Error?, serviceName: String) {
        presenter?.getIPCFail(message: error?.localizedDescription ?? "common.errorGetInformation".localize)
    }
}
