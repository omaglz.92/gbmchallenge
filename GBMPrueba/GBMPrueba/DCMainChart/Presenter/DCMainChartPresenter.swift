//
//  DCMainChartPresenter.swift
//  GBMPrueba
//
//  Created by omar hernandez on 21/08/21.
//  
//
import Foundation
final class DCMainChartPresenter: DCMainChartPresenterProtocol {
    var view: DCMainChartViewProtocol?
    var interactor: DCMainChartInteractorInputProtocol?
    var router: DCMainChartRouterProtocol?
    var dataArrayIPC: DCGBMResponse?
    private var timer: Timer?
    init() {
        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(getIPC), userInfo: nil, repeats: true)
    }
    @objc func getIPC() {
        view?.showWait()
        interactor?.getIPC()
    }
}
extension DCMainChartPresenter: DCMainChartInteractorOutputProtocol {
    func getIPCSuccess(entity: DCGBMResponse) {
        self.dataArrayIPC = entity
        view?.hideWait()
        view?.getIPCSuccess()
    }
    func getIPCFail(message: String) {
        view?.hideWait()
    }
}
