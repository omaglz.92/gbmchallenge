//
//  DCMainChartRouter.swift
//  GBMPrueba
//
//  Created by omar hernandez on 21/08/21.
//  
//
import UIKit
final class DCMainChartRouter: DCMainChartRouterProtocol {
    var view: DCMainChartVC
    private var presenter: DCMainChartPresenter
    private var interactor: DCMainChartInteractor
    init() {
        self.view = DCMainChartVC()
        self.presenter = DCMainChartPresenter()
        self.interactor = DCMainChartInteractor()
        view.presenter = self.presenter
        presenter.view = self.view
        presenter.interactor = self.interactor
        presenter.router = self
        interactor.presenter = self.presenter
    }
}

