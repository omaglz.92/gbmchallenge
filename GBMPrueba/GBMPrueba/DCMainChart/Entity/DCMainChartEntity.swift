//
//  DCMainChartEntity.swift
//  GBMPrueba
//
//  Created by omar hernandez on 21/08/21.
//  
//
// MARK: - DCGBMIpc
struct DCGBMResponseElement: Codable {
    let date: String
    let price: Double
    let percentageChange: Double
    let volume: Int
    let change: Double
}
// MARK: - DCGBMIpcResponse
typealias DCGBMResponse = [DCGBMResponseElement]
