//
//  DCMainChartProtocol.swift
//  GBMPrueba
//
//  Created by omar hernandez on 21/08/21.
//  
//
import UIKit
protocol DCMainChartViewProtocol: DCViewBaseProtocols {
    func getIPCSuccess()
}
protocol DCMainChartPresenterProtocol {
    var dataArrayIPC: DCGBMResponse? { get set }
    func getIPC()
}
protocol DCMainChartRouterProtocol {
}
protocol DCMainChartInteractorInputProtocol {
    func getIPC()
}
protocol DCMainChartInteractorOutputProtocol {
    func getIPCSuccess(entity: DCGBMResponse)
    func getIPCFail(message: String)
}

