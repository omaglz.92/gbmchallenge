//
//  DCAuthProtocol.swift
//  GBMPrueba
//
//  Created by omar hernandez on 21/08/21.
//  
//
import UIKit
protocol DCAuthPresenterProtocol {
    func showChart()
}
protocol DCAuthRouterProtocol {
    func showChart()
}

