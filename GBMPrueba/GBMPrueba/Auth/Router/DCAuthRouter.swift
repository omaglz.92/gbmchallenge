//
//  DCAuthRouter.swift
//  GBMPrueba
//
//  Created by omar hernandez on 21/08/21.
//  
//
import UIKit
final class DCAuthRouter {
    var view: DCAuthVC
    private var presenter: DCAuthPresenter
    init() {
        self.view = DCAuthVC()
        self.presenter = DCAuthPresenter()
        view.presenter = self.presenter
        presenter.router = self
    }
}
extension DCAuthRouter: DCAuthRouterProtocol {
    func showChart() {
        let router = DCMainChartRouter()
        view.navigationController?.pushViewController(router.view, animated: true)
    }
}
