//
//  DCAuthVC.swift
//  GBMPrueba
//
//  Created by omar hernandez on 21/08/21.
//  
//
import UIKit
import LocalAuthentication
final class DCAuthVC: DCBaseVC {
    @IBOutlet private weak var contentBiometrics: UIView!
    @IBOutlet private weak var imgBiometrics: UIImageView!
    let biometrics = DCBiometrics.shared
    var presenter: DCAuthPresenter?
    override func viewDidLoad() {
        super.viewDidLoad()
        biometrics.delegate = self
        if biometrics.canUseFaceID() {
            imgBiometrics.image = UIImage(named: "ic_faceID")
        } else {
            imgBiometrics.image = UIImage(named: "ic_touchID")
        }
        configureButtonView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    func configureButtonView() {
        let cornerRadius = contentBiometrics.bounds.height/2
        contentBiometrics.layer.cornerRadius = cornerRadius
        contentBiometrics.layer.shadowColor = UIColor.DCShadow.cgColor
        contentBiometrics.layer.shadowOpacity = 1
        contentBiometrics.layer.shadowOffset = .zero
        contentBiometrics.layer.shadowRadius = 4
    }
    @IBAction func touchBiometrics(_ sender: Any) {
        biometrics.authenticate(message: "", fallbackTitle: "", cancelTitle: "")
    }
}
extension DCAuthVC: DCBiometricsDelegate {
    func authenticationSuccess() {
        presenter?.showChart()
    }
    func authenticationFail(error: Error?) {}
}

