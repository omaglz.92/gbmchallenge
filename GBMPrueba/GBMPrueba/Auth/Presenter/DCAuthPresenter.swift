//
//  DCAuthPresenter.swift
//  GBMPrueba
//
//  Created by omar hernandez on 21/08/21.
//  
//
final class DCAuthPresenter: DCAuthPresenterProtocol {
    var router: DCAuthRouterProtocol?
    func showChart() {
        router?.showChart()
    }
}
